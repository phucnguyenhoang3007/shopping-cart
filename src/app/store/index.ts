import { ActionReducerMap } from '@ngrx/store';
import { CartState } from './cart/cart.states';
import { cartReducer } from './cart/cart.reducer';

export interface AppState {
  cart: CartState;
}

export const reducers: ActionReducerMap<AppState> = {
  cart: cartReducer,
};
