import { createReducer, on } from '@ngrx/store';
import {
  addProductToCart,
  updateQuantityInCart,
  removeProductFromCart,
  loadCartFromLocalStorage,
} from './cart.actions';
import { CartState } from './cart.states';

const initialState: CartState = {
  items: [],
};

export const cartReducer = createReducer(
  initialState,
  on(addProductToCart, (state, { product, quantity }) => {
    const existItem = state.items.find(
      (item) => item.product.id === product.id
    );
    if (existItem) {
      return {
        ...state,
        items: state.items.map((item) => {
          return item.product.id === product.id
            ? { ...item, quantity: item.quantity + quantity }
            : item;
        }),
      };
    }
    return {
      ...state,
      items: [
        ...state.items,
        {
          product,
          quantity,
        },
      ],
    };
  }),

  on(removeProductFromCart, (state, { productId }) => {
    return {
      ...state,
      items: state.items.filter((item) => item.product.id !== productId),
    };
  }),
  on(loadCartFromLocalStorage, (state, { cart }) => {
    return {
      ...state,
      items: cart.items,
    };
  }),
  on(updateQuantityInCart, (state, { productId, quantity }) => {
    return {
      ...state,
      items: state.items.map((item) =>
        item.product.id === productId ? { ...item, quantity: quantity } : item
      ),
    };
  })
);
