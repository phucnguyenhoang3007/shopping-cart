import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from '..';
import { Store } from '@ngrx/store';
import { map, tap } from 'rxjs';
import {
  addProductToCart,
  removeProductFromCart,
  updateQuantityInCart,
  loadCartFromLocalStorage,
} from './cart.actions';

@Injectable()
export class CartEffect {
  constructor(private actions$: Actions, private store: Store<AppState>) {}

  saveCart$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addProductToCart, removeProductFromCart, updateQuantityInCart),
        tap(() => {
          this.store.select('cart').subscribe((cart) => {
            localStorage.setItem('cart', JSON.stringify(cart));
          });
        })
      ),
    { dispatch: false }
  );

  loadCart$ = createEffect(() =>
    this.actions$.pipe(
      ofType('@ngrx/effects/init'),
      map(() => {
        const cart = localStorage.getItem('cart');

        if (cart) {
          return loadCartFromLocalStorage({ cart: JSON.parse(cart) });
        }
        return loadCartFromLocalStorage({ cart: { items: [] } });
      })
    )
  );
}
