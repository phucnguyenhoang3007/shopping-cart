import { createAction, props } from '@ngrx/store';
import { CartState } from './cart.states';

export const addProductToCart = createAction(
  '[Cart] Add Product',
  props<{ product: Product; quantity: number }>()
);

export const removeProductFromCart = createAction(
  '[Cart] Delete product',
  props<{ productId: number }>()
);

export const updateQuantityInCart = createAction(
  '[Cart] Update quantity',
  props<{ productId: number; quantity: number }>()
);

export const loadCartFromLocalStorage = createAction(
  '[Cart] Load cart from localStorage',
  props<{ cart: CartState }>()
);
