import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  standalone: true,
  imports: [],
  templateUrl: './pagination.component.html',
  styleUrl: './pagination.component.css',
})
export class PaginationComponent {
  @Input() currentPage: number = 0;
  @Output() currentPageChange = new EventEmitter<number>();

  @Input() totalItems: number = 0;
  @Input() totalPages: number = 0;
  itemsPerPage: number = 10;
  pagesToShow: number = 5;

  getPaginationRange(): (string | number)[] {
    const start = Math.max(
      1,
      this.currentPage - Math.floor(this.pagesToShow / 2)
    );
    const end = Math.min(this.totalPages, start + this.pagesToShow - 1);

    let pages: (string | number)[] = [];
    for (let i = start; i <= end; i++) {
      pages.push(i);
    }
    if (start > 1) {
      pages = [1, '...'].concat(pages);
    }
    if (end < this.totalPages) {
      pages = pages.concat(['...', this.totalPages]);
    }

    return pages;
  }

  onPageChange(page: number) {
    this.currentPageChange.emit(page);
  }
}
