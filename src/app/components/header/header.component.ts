import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart/cart.states';
import { Observable } from 'rxjs';
import { AppState } from '../../store';

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [RouterModule, CommonModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.css',
})
export class HeaderComponent {
  cart$: Observable<CartState>;

  constructor(private store: Store<AppState>) {
    this.cart$ = this.store.select('cart');
  }
}
