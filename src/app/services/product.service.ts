import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private url = 'http://localhost:3000/products';
  constructor(private http: HttpClient) {}

  getAllProducts(
    filter: string,
    currentPage: number
  ): Observable<PageResponse> {
    const params = {
      _page: currentPage,
    };
    if (filter === 'all')
      return this.http.get<PageResponse>(this.url, { params });
    return this.http.get<PageResponse>(this.url, {
      params: { ...params, category: filter },
    });
  }

  getProductBySlug(slug: string): Observable<Product[]> {
    return this.http.get<Product[]>(this.url, { params: { slug } });
  }
  getFeturedProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this.url, { params: { _limit: '8' } });
  }
}
