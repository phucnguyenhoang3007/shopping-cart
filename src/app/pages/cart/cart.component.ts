import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { CartItem, CartState } from '../../store/cart/cart.states';
import { Store } from '@ngrx/store';
import { AppState } from '../../store';
import { FormsModule } from '@angular/forms';

import {
  removeProductFromCart,
  updateQuantityInCart,
} from '../../store/cart/cart.actions';

@Component({
  selector: 'app-cart',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './cart.component.html',
  styleUrl: './cart.component.css',
})
export class CartComponent {
  cart$: Observable<CartState>;

  constructor(private store: Store<AppState>) {
    this.cart$ = this.store.select('cart');
  }

  quantityChange(item: CartItem, quantity: number) {
    if (quantity <= 0) {
      return;
    }
    this.store.dispatch(
      updateQuantityInCart({ productId: item.product.id, quantity })
    );
  }

  calculateTotalPrice(cart: CartState) {
    return cart.items.reduce((total, item) => {
      return total + item.product.price * item.quantity;
    }, 0);
  }

  removeProduct(productId: number) {
    this.store.dispatch(removeProductFromCart({ productId }));
  }
}
