import { CommonModule } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { CartState } from '../../store/cart/cart.states';
import { addProductToCart } from '../../store/cart/cart.actions';

@Component({
  selector: 'app-product-detail',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './product-detail.component.html',
  styleUrl: './product-detail.component.css',
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  quantity: number = 1;
  product: Product | undefined = undefined;
  private routeSubscription: Subscription | undefined;
  private productSubscription: Subscription | undefined;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private store: Store<{ cart: CartState }>
  ) {}

  ngOnInit() {
    this.routeSubscription = this.route.paramMap.subscribe((params) => {
      const slug = params.get('slug');
      if (slug) {
        this.productSubscription = this.productService
          .getProductBySlug(slug)
          .subscribe((product) => {
            this.product = product[0];
          });
      }
    });
  }

  ngOnDestroy() {
    if (this.routeSubscription) {
      this.routeSubscription.unsubscribe();
    }
    if (this.productSubscription) {
      this.productSubscription.unsubscribe();
    }
  }

  addProductToCart() {
    this.store.dispatch(
      addProductToCart({ product: this.product!, quantity: this.quantity })
    );
    window.alert('Add product successfully');
  }
}
