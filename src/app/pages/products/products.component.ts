import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import { ProductComponent } from '../../components/product/product.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ProductService } from '../../services/product.service';
import { Subscription } from 'rxjs';
import { CategoryService } from '../../services/category.service';
import { PaginationComponent } from '../../components/pagination/pagination.component';

@Component({
  selector: 'app-products',
  standalone: true,
  imports: [ProductComponent, RouterModule, CommonModule, PaginationComponent],
  templateUrl: './products.component.html',
  styleUrl: './products.component.css',
})
export class ProductsComponent implements OnInit, OnDestroy {
  subscriptions: Subscription = new Subscription();
  products: Product[] = [];
  categories: Category[] = [];
  filter: string = 'all';

  private _currentPage: number = 1;
  totalItems: number = 0;
  totalPages: number = 0;

  constructor(
    private productService: ProductService,
    private categoryService: CategoryService
  ) {}

  get currentPage(): number {
    return this._currentPage;
  }

  set currentPage(value: number) {
    if (value != this._currentPage) {
      this._currentPage = value;
      this.getAllProducts();
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
  }

  ngOnInit() {
    this.subscriptions.add(
      this.categoryService
        .getAllCategories()
        .subscribe((categories) => (this.categories = categories))
    );
    this.getAllProducts();
  }

  setFilter(filter: string) {
    this.filter = filter;
    this.getAllProducts();
  }

  getAllProducts() {
    this.subscriptions.add(
      this.productService
        .getAllProducts(this.filter, this._currentPage)
        .subscribe((response) => {
          this.products = response.data;
          this.totalItems = response.items;
          this.totalPages = response.pages;
        })
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
